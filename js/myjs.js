
$(document).ready(function(){

        $('.menu-btn').click(function () {
            $('nav div.navbar').slideToggle();
        });
});

$(function(){
    var	btn = $(".slider__btn");

    btn.on("click",function(){
        $(".slider__item").first().clone().appendTo(".slider");
        $(".slider__image").first().css({transform: "rotateX(-180deg)", opacity: 0});
        setTimeout(function(){
            $(".slider__item").first().remove();
        },200);
    });
});

$(document).ready(function(){

    $(function(){

        $('.header .container .slides:gt(0)').hide();
        setInterval(function(){
            $('.header .container :first-child').fadeOut(2000).next('.header .container .slides').fadeIn(2000)
                .end().appendTo('.header .container');
        }, 4000);

    });

});

$(document).ready(function(){

    $('#adv').hover(function () {
        $('#adv-a').fadeOut(400);
        $('#adv-img').fadeIn(600);
    },function () {
        $('#adv-a').fadeIn(600);
        $('#adv-img').fadeOut(400);
    });

});


$('.question').click(function () {
    var qID = $(this).attr('id');
    $('#' + qID + ' .a').slideToggle(200);
    var p = $('#' + qID + ' .i').html();
    if (p == '<i class="flaticon-round-add-button"></i>') {
        $('#' + qID + ' .i').html('<i class="flaticon-negative-sign-inside-circle"></i>');
    } else {
        $('#' + qID + ' .i').html('<i class="flaticon-round-add-button"></i>');
    }
});
$(".qManage .qm,.question .r").click(function (e) {
    e.stopPropagation();
});



$(document).ready(function(){

    $('.hidshow').click(function () {
        var tID = $(this).attr('id');
        $('#' + tID + ' .togl').slideToggle(400);
        var p = $('#' + tID + ' .i').html();
        if (p == '<img class="icon" src="img/candle.png" alt="شیوه صحیح پرورش کودکان و نوجوانان">') {
            $('#' + tID + ' .i').html('<img class="icon" src="img/flag.png" alt="شیوه صحیح پرورش کودکان و نوجوانان">');
        } else {
            $('#' + tID + ' .i').html('<img class="icon" src="img/candle.png" alt="شیوه صحیح پرورش کودکان و نوجوانان">');
        }

    });

});


$(document).ready(function(){
    $('.hidshow-att').click(function () {
        var aID = $(this).attr('id');
        $('#' + aID + ' .togl-att').slideToggle(400);
        var p = $('#' + aID + ' .i').html();
        if (p == '<img class="icon" src="img/target.png" alt="آموزش قانون جذب به کودکان و نوجوانان">') {
            $('#' + aID + ' .i').html('<img class="icon" src="img/magnet.png" alt="بهترین مدرس قانون جذب">');
        } else {
            $('#' + aID + ' .i').html('<img class="icon" src="img/target.png" alt="آموزش قانون جذب به کودکان و نوجوانان">');
        }

    });

});