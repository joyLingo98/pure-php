<body>

<div >
    <img class="menu-btn" src="img/options.png" alt="">
    <header class="menu z-depth-1-half bg-basecolor">
        <nav>
            <div class="navbar">
                <ul>
                    <li><a id="a1" href="<?php echo KA_HOME_URL; ?>">خانه</a></li>
                    <li><a id="a2" href="<?php echo KA_HOME_URL; ?>?page=books">قصه ها</a></li>
                    <li><a id="a3" href="<?php echo KA_TUTORIAL_URL; ?>">پرورش</a></li>
                    <li><a id="a4" href="<?php echo KA_HOME_URL; ?>?page=attraction">قانون جذب</a></li>
                    <li><a id="a5" href="<?php echo KA_QUESTION_URL; ?>">پرسش و پاسخ</a></li>
                    <li><a id="a6" href="<?php echo KA_HOME_URL; ?>?page=contact">تماس با ما</a></li>
                </ul>
            </div>
        </nav>
    </header>