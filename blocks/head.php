<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $seo["title"]; ?></title>
    <meta name="author" content="www.weblic.ir">
    <meta name="web_author" content="مریم علیشاهی 09126337937">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="theme-color" content="#e0be00" />
    <meta name="description" content="<?php echo $seo["description"]; ?>">
    <link rel="icon" href="img/sunny.png" type="image/x-icon">
    <link href="font/font/flaticon.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="css/animate.css">



    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Your custom styles (optional) -->
    <?php

    if (!ISSET($_GET["page"]))
    {
        echo '<link href="css/style.css" rel="stylesheet">';
    }
    else {
        echo '<link href="css/styleblue.css" rel="stylesheet">';
    }
    ?>
    <!-- JQuery -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>

</head>
