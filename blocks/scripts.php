<!-- SCRIPTS -->
<!-- Bootstrap tooltips -->
<script src="js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- My Js -->
<script src="js/myjs.js"></script>
<?php if (isAdmin()): ?>
<script src="js/admin.js"></script>
<?php endif; ?>
</body>

</html>
