<div class="header">

    <div class="container">
        <h4 id="motto1" class="slides wow pulse"> شادی بهترین داروی دنیا برای هر دردیست !</h4>

        <h4 id="motto2" class="slides wow pulse">موفقیت با گفتن من می تونم ساخته میشه، نه نمی تونم!</h4>

        <h4 id="motto3" class="slides wow pulse">وقتی زندگیت آفتابی نیست ، خودت آفتاب زندگیت شو!</h4>
        <h4 id="motto4" class="slides wow pulse">امید که زنده باشه ، درد رو از بین می بره!</h4>
        <h4 id="motto5" class="slides wow pulse">من میتونم و انجامش میدم! هوامو داشته باش</h4>

    </div>

</div>