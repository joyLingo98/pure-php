<section class="page bg-tutorial-primary">
    <div class="contner-fluid oth-pages ">
        <div class="container">
            <div class="align-right padding-top">
                <?php if (isAdmin()): ?>
                    <div class="success ">
                        <span class="adminName">خوش آمدی <?php echo KA_ADMIN_DISPLAYNAME; ?> عزیز</span>
                        <a class="logout" href="<?php echo KA_QUESTION_URL . '&logout=1'; ?>">خروج</a>
                    </div>
                <?php endif; ?>
                <h1 class="color-tutorial-secondary text-shadow-lower wow slideInUp">
                    سوالات خود را از کارشناسان ما رایگان بپرسید
                </h1>
                <p class="margin-buttom">
                    اگر در <a class="a-none-color sun" href="<?PHP echo KA_ADDRESS_TELEGRAM ?>">کانال کودک آفتاب</a> عضو هستید که بخوبی روانشناسان و کارشناسان مشاوره ما را میشناسید ، پس سوال خود را بپرسید و در انتها نام کارشناسی که میخواهید سوال از او پرسیده شود را ذکر نمایید . در کوتاهترین زمان ممکن به سوال شما پاسخ داده خواهد شد. اگر با
                    <a class="a-none-color non-hover" href="<?php echo KA_HOME_URL; ?>?page=login">کارشناسان</a> و روانشناسان ما آشنایی ندارید و یا به همه کارشناسان ما اعتماد دارید ، تنها سوال خود را بپرسید ، باز هم در کوتاهترین زمان به سوال شما پاسخ داده میشود.

                </p>
            </div>
            <?php
            if ($errorMsg) {
                echo "<div class='errSuc error'>" . nl2br($errorMsg) . "</div>";
            } elseif ($successMsg) {
                echo "<div class='errSuc success'>" . nl2br($successMsg) . "</div>";
            }
            ?>
        </div>

        <div class="ques-main">
            <div class="row title-ques">
                <div class="col-md-4 col-sm-5 col-xs-6">
                    <h2>
                        سامانه پرسش و پاسخ <a class="a-none-color" href="<?php echo KA_HOME_URL; ?>">کودک آفتاب</a>
                    </h2>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-6">
                    <form action="#" method="get">
                        <script>document.querySelector("form").setAttribute("action", "")</script>
                        <div class="form-row">
                            <input type="hidden" name="page" value="questions" /> <!-- ali's trick when action didn't work    -->
                            <div class="form-group">
                                <input class="form-control" type="text" name="search" id="s"/>
                            </div>
                            <div class="form-group">
                                <select name="status" class="form-control">
                                    <option value="all">همه ی سوالات</option>
                                    <?php if (isAdmin()): ?>
                                        <option value="pending">منتظر تائید</option>
                                    <?php endif; ?>
                                    <option value="published">بدون پاسخ</option>
                                    <option value="answered">پاسخ داده شده</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-search" ></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="clear"></div>
            </div>

            <div>
                <aside id="add-ques">
                    <form action="#" method="post">
                        <script>document.querySelector("form").setAttribute("action", "")</script>
                        <div class="form-group searchform">
                            <input type="text" class="form-control" placeholder="نام کامل شما" name="uName">
                        </div>
                        <div class="form-group searchform">
                            <input type="text" class="form-control" placeholder="چند سال دارید" name="uAge">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="ایمیل شما" name="uMail">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="شماره موبایل شما" name="uPhone">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="uText" placeholder="متن سوال شما"></textarea>
                        </div>
                        <input type="submit" name="submitQuestion" value="ارسال سوال" class="btn btn-ques z-depth-1-half">
                    </form>
                </aside>

                <div class="ques-answ">
                    <h4>سوالات شما از کارشناسان کودک آفتاب  :</h4>

                    <?php
                    if ($questions == null) {
                        echo '
                        <div class="error">موردی یافت نشد<br><br>
                            <a href="'.KA_HOME_URL.'">بازگشت به صفحه اصلی</a>
                        </div>';
                    } else{
                        $num=0;
                        foreach ($questions as $q):
                        $num+=2;
                        ?>

                        <div class="question row <?php echo $q['status'] ?> wow zoomIn" data-wow-delay="<?php echo $num; ?>00ms" id="q-<?php echo $q['id'] ?>">
                            <div class="q col-md-9 col-sm-8">
                                <span class="i"><i class="flaticon-round-add-button"></i></span>
                                <?php echo $q['uname'] ?>
                                <p><?php echo per_number($q['utext']) ?></p>
                            </div>
                            <?php if (isAdmin()): ?>
                                <div class="col-md-3 col-sm-4 qManage">
                                    <span class="result" style="display: none">...</span>
                                    <span class="flaticon-remove-button qm" id="qmd-<?php echo $q['id'] ?>" title="حذف"></span>
                                    <?php if ($q['status'] != 'answered') { ?>
                                        <span class="flaticon-faqs qmr" title="افزودن پاسخ"></span>
                                        <span class="flaticon-confirm qm" id="qmpu-<?php echo $q['id'] ?>" title="تائید"></span>
                                        <span class="flaticon-time-left qm" id="qmpe-<?php echo $q['id'] ?>" title="لغو تائید"></span>
                                    <?php } ?>
                                </div>
                            <?php endif; ?>
                            <div class="col-md-12 r">
                                <form action="#" method="post" class="replyForm">
                                    <script>document.querySelector("form").setAttribute("action", "")</script>
                                    <input name="qid" value="<?php echo $q['id'] ?>" type="hidden"/>
                                    <textarea name="atext" rows="5" placeholder="پاسخ دهید ..."></textarea>
                                    <input type="submit" name="submitAnswer" class="btn btn-ques submit" value="ارسال پاسخ"/>
                                </form>
                            </div>
                            <div class="clear"></div>
                            <?php
                            if ($q['status'] == 'answered') {
                                echo getAnswers($q['id']);
                            } else if ($q['status'] == 'published') {
                                echo '<div class="a">هنوز به این سوال پاسخ داده نشده است !</div>';
                            } else if ($q['status'] == 'pending' and isAdmin()) {
                                echo '<div class="a">این سوال هنوز تائید نشده است.</div>';
                            }
                            ?>
                        </div>

                    <?php endforeach;  } ?>
                </div>

                <div class="clear"></div>
            </div>
        </div>
        <div class="pagination">
            <?php $numPages = getNumPages($numQuestions); ?>
            <a href="<?php echo getPageUrl(1); ?>">«</a>
            <?php for ($i = 1; $i <= $numPages; $i++) {
                if ($i == $qpn) {
                    echo "<strong>$i</strong>";
                } else {
                    echo "<a href='".getPageUrl($i)."'>$i</a>";
                }
            } ?>
            <a href="<?php echo getPageUrl($numPages); ?>">»</a>
            <div class="clear"></div>
        </div>

    </div>
</section>
