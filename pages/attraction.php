<section class="page bg-attraction-primary">
    <div class="contner-fluid oth-pages attraction ">
        <div class="container">
            <div class="align-right">
                <h1 class="color-attraction-secondary text-shadow-lower wow fadeInDownBig">
                    پادشاه یا ملکه زندگی خودت باش!
                </h1>
                <p>
                    راستش بین انتخاب اسم <strong class="hover-theme">مثبت اندیشی</strong> یا <strong class="hover-theme">قانون جذب</strong> برای این صفحه تردید داشتم. چیزی که من و افراد بسیار زیادی توی دنیا راجع بهش به یقین رسیدیم اینه که هر چقدر بیشتر راجع به خودت ، اطرافیانت و دنیا فکرهای خوب تو سرت باشه ، چیزهای بهتری سر راهت میاد. انگار دنیا برای آدم های سپاسگزار پارتی بازی میکنه و اتفاقات و آدم های خوب رو سر راهشون میاره . موضوع جالب تر میشه وقتی این سپاسگزاری از خودمون باشه ، یعنی از خودمون متچکر باشیم یعنی خودمون رو واقعا و از ته قلب دوست داشته باشیم و یا به عبارت دیگه خودمون رو باور داشته باشیم!
                </p>
                <p>
                    <strong class="hover-theme">خودم را باور داشتم!!!</strong>
                </p>
                <p>
                    چقدر این جمله رو از دهن آدم های موفق شنیدید؟ از این به بعد هم خیلی این جمله رو هم زیاد خواهید شنید. موفقیت هم مثل ریاضی ، مثل فیزیک فرمول داره ، راه داره ! اولین فرمول برای موفقیت اینه که من خودم رو باور داشته باشم و مطمئن باشم از پسش برمیام . من عاشق خودم هستم و لیاقت بهترین زندگی و بهترین شرایط و بهترین دوستان رو دارم ، من در قبال خودم مسئول هستم ، مسئول خوشبخت کردن خودم! من از خودم و همه تلاش هایی که برای خوشحالی خودم میکنم سپاسگزارم! و وقتی به این حس درونی رسیدم دنیا هم شروع میکنه به پارتی بازی کردن برای دادن همه چیزهای خوب به من!
                </p>
                <p>
                    ما همونی میشیم که باور داریم . پس اینجا ، توی این صفحه قرارمون اینه که هر باور منفی و غیر مفیدی رو که داریم دور بریزیم و بجاش مغز و قلبمون پر کنیم از چیزهای مثبت و خوب! و بعد معجزه های دنیا برای ما شروع میشن ...
                </p>
                <p>
                    ما تبدیل میشیم به یک آدم دوست داشتنی ، زیبا ، موفق و شاد ...
                </p>
                <p>
                    <strong>
                        لازم به یادآوری است تمام مطالب این صفحه و حتی بسیار کامل تر آنها بصورت ویس در <a href="<?PHP echo KA_ADDRESS_TELEGRAM; ?>">کانال تگرامی کودک آفتاب</a> موجود میباشد . پس برای پیوستن به
                        <a href="<?PHP echo KA_ADDRESS_TELEGRAM; ?>">کانال کودک آفتاب</a> تردید نکنید.
                    </strong>
                </p>

                <div id="success"></div>
            </div>
            <div class="search-tut">
                <form action="" method="get">
                    <div class="form-row">
                        <input type="hidden" name="page" value="attraction" />
                        <div class="form-group">
                            <input class="form-control" type="text" name="searchatt" id="s" placeholder="جستجو"/>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-search" ></button>
                        </div>
                    </div>
                </form>
            </div>
            <?php
            if ($attraction == null) {
                echo '
                        <div class="error">موردی یافت نشد<br><br>
                            <a href="'.KA_HOME_URL.'">بازگشت به صفحه اصلی</a>
                        </div>';
            } else {
                $num=0;
                foreach ($attraction as $at):
                    $num+=2;
                    ?>
                    <article class="tutorial-part attraction-part hidshow-att wow fadeInRight" data-wow-delay="<?php echo $num; ?>00ms" id="a-<?php echo $at['id'] ?>" >

                        <h2>
                            <span class="i"><img class="icon" src="img/target.png" alt="آموزش قانون جذب به کودکان و نوجوانان"></span>
                            <strong><?php echo $at['title'] ?></strong>
                        </h2>

                        <div class="togl-att descdesign">
                            <div class="desc stars">
                                <?php echo per_number($at['descrip']); ?>
                            </div>
                            <p class="details master-name ltr-dir">
                                <?php echo $at['master_name'] ?>
                            </p>
                            <p class="details date">

                                <?php echo jdate(KA_DATE_FORMAT, strtotime($at['created_date'])) ?>
                            </p>
                            <div class="clear"></div>

                            <div class="share">
                                <p>
                                    <span class="share-spri"></span>
                                    <strong class="text-shadow-lower">این صفحه را با دیگران به اشتراک بگذارید</strong>
                                </p>
                                <ul>
                                    <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A//plus.google.com/share?url=http%253A//kodakeaftab.ir/index.php?page=attraction&title=<?php echo $at['title'] ?>&summary=<?php echo strip_tags(substr($at['descrip'],0,100)); ?>&source="><img class="icon" src="../img/linkedin.png" alt="کودک آفتاب در لینکدین"></a></li>
                                    <li><a href="https://telegram.me/share/url?url=http://kodakeaftab.ir/index.php?page=attraction&text=<?php echo $at['title'] ?>"><img class="icon" src="../img/telegram.png" alt="کانال تلگرامی کودک آفتاب"></a></li>
                                    <li><a href="https://plus.google.com/share?url=http%3A//kodakeaftab.ir/index.php?page=attraction"><img class="icon" src="../img/google.png" alt="کودک آفتاب در گوگل"></a></li>
                                </ul>
                            </div>
                            <?php
                            if ($at['prac']){ ?>
                                <div class="tamrin z-depth-1-half bg-attraction-secondary color-tutorial-primary">
                                    <h3><strong class="text-shadow-lower">با بازی خودت رو قوی کن :</strong></h3>
                                    <p class="t-desc text-shadow-lower"><?php echo $at['prac'] ?></p>
                                </div>
                            <?php } ?>
                            <div class="winner"></div>
                        </div>
                        <div class="hr"></div>
                    </article>
                <?php endforeach; } ?>

            <div class="pagination">
                <?php $numPages = getNumPagesAtt($numAttraction); ?>
                <a href="<?php echo getPageUrlAtt(1); ?>">«</a>
                <?php for ($i = 1; $i <= $numPages; $i++) {
                    if ($i == $apn) {
                        echo "<strong>$i</strong>";
                    } else {
                        echo "<a href='".getPageUrlAtt($i)."'>$i</a>";
                    }
                } ?>
                <a href="<?php echo getPageUrlAtt($numPages); ?>">»</a>
                <div class="clear"></div>
            </div>
        </div>

    </div>
</section>



