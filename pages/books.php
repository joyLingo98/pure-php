<section class="page bg-orane">
    <div class="contner-fluid picbook">
        <div class="container">
            <div class="align-right text-shadow-lower wow lightSpeedIn">
                <h1>
                    کتابخانه صوتی و تصویری <a href="<?php echo KA_HOME_URL; ?>">کودک آفتاب</a>
                </h1>
            </div>
            <div class="card-deck">
                <?php
                $num=4;
                foreach ($allBooks as $ab):
                $num+=1;
                ?>

                <div class="col-xl-3 col-sm-6 col-md-3 col-xs-12">
                    <div  class="card z-depth-1-half wow swing" data-wow-delay="<?php echo $num; ?>00ms">
                        <!--Card image-->
                        <div class="view overlay">
                            <img class="card-img-top z-depth-1-half" src="img/<?php echo $ab['image'] ?>" alt="کتاب داستان صوتی و تصویری رایگان <?php echo $ab['title'] ?>">
                        </div>
                        <!--Card content-->
                        <div class="card-body">
                            <!--Title-->
                            <a class="border-none" href="<?php echo KA_HOME_URL ?>?page=book&id=<?php echo $ab["id"]; ?>"><h4 class="card-title text-shadow-lower"><span>کتاب داستان </span><strong><?php echo $ab['title'] ?></strong></h4></a>
                            <!--Text-->
                            <p class="card-text text-white"><?php echo per_number($ab['description']) ?></p>
                            <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                            <a class="btn z-depth-1-half" href="<?php echo KA_HOME_URL ?>?page=book&id=<?php echo $ab["id"]; ?>">
                                خواندن قصه
                            </a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>



        <div class="clear"></div>
    </div>
</section>