<section class="page bg-tutorial-primary">
    <div class="contner-fluid oth-pages ">
        <div class="container">
            <div class="align-right">
                <h1 class="color-tutorial-secondary text-shadow-lower wow fadeInDownBig">
                    پرورش توانایی های کودکان و نوجوانان
                </h1>
                <p>
                    قدیم تر ها والدین با زور اصرار و هر راهی که به ذهنشان می رسید ، میخواستند فرزندشان همانی شود که آنها میخواهند. به قول خودمان همه یا دکتر می خواستند یا مهندسی چیزی...
                </p>
                <p>
                    حالا اما خدا رو شکر ذهن اغلب پدر و مادر ها باز شده است ، دیگر این فریب قشنگ بچه مهندس و دکتر داشتن ، رنگ باخته است. اما خب هنوزم مشکلاتی هست ! یکی از سوالاتی که بسیار از من پرسیده میشود اینست که چطور بدانم استعداد کودک و یا نوجوان من در چه زمینه ایی است که بخواهم آن را پرورش دهم؟! والدین بسیاری از این شکایت دارند که فرزند ما خودش هم نمی داند چه دوست دارد ، گاها شده با اصرار به ما گفته که مثلا کلاس موسیقی دوست دارد ، او را در کلاس موسیقی ثبت نام کرده ایم اما بعد از مدت کوتاهی خسته شده و گفته نه من این ساز را دوست ندارم و چه چه....
                </p>
                <p>
                    و جواب من تنها یک چیز است ، خودتان و فرزندتان آگاه بمانید ، جالب است که ما برای همه چیز آموزش می بینیم اما برای موفق شدن و رسیدن به اهدافمان هیچ آموزشی نمی بینیم . آموزش ببینید و کتاب بخوانید آنگاه خواهید دید که در همان زمان که باید فرزندتان و استعدادش شکوفا میشود ، طوری که خودتان هم انگشت به دهان می مانید!
                </p>
                <p>
                    <strong>
                        و اما اولین قدم برای موفقیت داشتن روح و روان سالم است.
                    </strong>
                </p>
                <p>
                    فرزندی که ذهنش درگیر مشکلات و ناگفته ها و غم و خشم درونی نباشد ، روح و ذهنش یکی میشود و آماده می شود برای اوج گرفتن!!!
                </p>
                <p>
                    در این بخش به کمک شما می آییم تا نیاز های فرزندتان را بشناسید و بستر را مهیا کنید برای سلامت و آرامش ذهنش ، قول میدهم با پیگیری درس های این بخش ذهن خودتان هم آرام تر شود چه برسد به فرزندتان ...
                </p>
                <p>
                    <a class="a-none-color" href="<?php echo KA_HOME_URL; ?>?page=attraction">ذهن که ساکت شد می رویم سراغ صفحه بعد یعنی مثبت اندیشی برای کودکان و نوجوانان و  ذهن ساکتشان را پر میکنیم از چیزهای خوب و اینجاست که آغاز پرواز است ...</a>
                </p>
                <p>
                    <strong>
                        لازم به یادآوریست که تمام آموزش های این سایت در <a class="sun a-none-color" href="<?PHP echo KA_ADDRESS_TELEGRAM ?>">کانال تلگرامی کودک آفتاب</a> بصورت ویس و گاها کاملتر موجود میباشند. پس برای عضویت در
                        <a class="sun a-none-color" href="<?PHP echo KA_ADDRESS_TELEGRAM ?>">کانال کودک آفتاب</a> تردید نکنید.

                    </strong>
                </p>
                <div id="lamp"></div>
            </div>
            <div class="search-tut">
                <form action="" method="get">
                    <div class="form-row">
                        <input type="hidden" name="page" value="tutorial" />
                        <div class="form-group">
                            <input class="form-control" type="text" name="searchtut" id="s" placeholder="جستجو"/>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-search" ></button>
                        </div>
                    </div>
                </form>
            </div>

            <?php
            if ($tutorial == null) {
                echo '
                        <div class="error">موردی یافت نشد<br><br>
                            <a href="'.KA_HOME_URL.'">بازگشت به صفحه اصلی</a>
                        </div>';
            } else{
                $num=0;
                foreach ($tutorial as $t):
                    $num+=1;
                    ?>

                    <article class="hidshow tutorial-part wow fadeInRight" data-wow-delay="<?php echo $num; ?>00ms" id="t-<?php echo $t['id'] ?>">
                        <div class="hr"></div>
                        <h2>
                            <span class="i"><img class="icon" src="img/candle.png" alt="شیوه صحیح پرورش کودکان و نوجوانان"></span>

                            <strong><?php echo $t['title'] ?></strong>
                        </h2>
                        <div class="togl descdesign">
                            <div class="desc stars">
                                <?php echo per_number($t['descrip']) ?>
                            </div>
                            <p class="details master-name ltr-dir">
                                <?php echo $t['master_name'] ?>
                            </p>
                            <p class="details date">
                                <?php echo jdate(KA_DATE_FORMAT, strtotime($t['created_date'])) ?>
                            </p>
                            <div class="clear"></div>
                            <div class="share">
                                <p>
                                    <span class="share-spri"></span>
                                    <strong class="text-shadow-lower">این صفحه را با دیگران به اشتراک بگذارید</strong>
                                </p>
                                <ul>
                                    <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=http%3A//kodakeaftab.ir/index.php?page=tutorial&title=<?php echo $t['title'] ?>&summary=<?php echo $t['descrip']; ?>&source="><img class="icon" src="../img/linkedin.png" alt="لینکدین مریم علیشاهی"></a></li>
                                    <li><a href="https://telegram.me/share/url?url=http://kodakeaftab.ir/index.php?page=tutorial&text=<?php echo $t['title'] ?>"><img class="icon" src="../img/telegram.png" alt="کانال تلگرام قانون جذب و روانشناسی"></a></li>
                                    <li><a href="https://plus.google.com/share?url=http%3A//kodakeaftab.ir/index.php?page=tutorial"><img class="icon" src="../img/google.png" alt=" مریم علیشاهی در گوگل"></a></li>
                                </ul>
                            </div>
                            <?php
                            if ($t['prac']){ ?>
                                <div class="tamrin z-depth-1-half bg-tutorial-secondary color-tutorial-primary">
                                    <h3><strong class="text-shadow-lower">با بازی خودت رو قوی کن :</strong></h3>
                                    <p class="t-desc text-shadow-lower"><?php echo $t['prac'] ?> </p>
                                </div>
                            <?php } ?>
                            <img class="rubik" src="../img/rubik.png" alt="بحران دوران بلوغ و روانشناسی">
                        </div>
                    </article>
                <?php endforeach; } ?>

            <div class="pagination">
                <?php $numPages = getNumPagesTut($numTutorial); ?>
                <a href="<?php echo getPageUrlTut(1); ?>">«</a>
                <?php for ($i = 1; $i <= $numPages; $i++) {
                    if ($i == $tpn) {
                        echo "<strong>$i</strong>";
                    } else {
                        echo "<a href='".getPageUrlTut($i)."'>$i</a>";
                    }
                } ?>
                <a href="<?php echo getPageUrlTut($numPages); ?>">»</a>
                <div class="clear"></div>
            </div>

        </div>
    </div>
</section>

