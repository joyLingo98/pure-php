<section class="page bg-contact">
    <div class="container-fluid oth-pages paddmarg">
        <h1 class="text-center text-light"><strong>ورود اعضا</strong></h1>
        <hr>
        <div class="row" id="contatti">
            <div class="container mt-5" >
                <?php
                if ($errorMsg) {
                    echo "<div class='errSuc error'>" . nl2br($errorMsg) . "</div>";
                } elseif ($successMsg) {
                    echo "<div class='errSuc success'>" . nl2br($successMsg) . "</div>";
                }
                ?>
                <div class="contact">
                    <div class="con-text">
                        <form action="" method="post">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" name="username" class="form-control mt-2" placeholder="نام کاربری" tabindex="2" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="password" name="password" class="form-control mt-2" placeholder="پسورد" tabindex="3" required>
                                    </div>
                                </div>
                                <div class="col-12 text-left">
                                    <input name="submitLogin" class="btn btn-outline-light" type="submit" tabindex="7" value="ورود">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

</section>
